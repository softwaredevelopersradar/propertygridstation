﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridStation.Model
{
    public enum Language : byte
    {
        [Description("Русский")] RU,
        [Description("English")] EN,
        [Description("Azərbaycan")] AZ,
    }

    public enum ConnectionType : byte
    {
        GSM = 0,
        Ethernet = 1,
        Radio = 2,
    }

    public enum AccessTypes : byte
    {
        Admin = 0,
        User = 1
    }

    public enum ViewCoord : byte
    {
        [Description("DD.dddddd")]
        Dd = 1,
        [Description("DD MM.mmmm")]
        DMm = 2,
        [Description("DD MM SS.ss")]
        DMSs = 3
    }
}

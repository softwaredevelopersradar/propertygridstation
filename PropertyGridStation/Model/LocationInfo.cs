﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using System.Net;

namespace PropertyGridStation.Model
{
    public class LocationInfo : AbstractModelWithCoordTranslation, IModelMethods<LocationInfo>
    {
        private double _distance = 20000;
        private double _angle = 0;

        [PropertyOrder(2)]
        [Range(10, Double.MaxValue)]
        public double Distance
        {
            get => _distance;
            set
            {
                if (_distance == value) return;
                _distance = value;
                OnPropertyChanged();
            }
        }

        [PropertyOrder(3)]
        [Range(0, 360)]
        public double Angle
        {
            get => _angle;
            set
            {
                if (_angle == value) return;
                _angle = value;
                OnPropertyChanged();
            }
        }

        public LocationInfo Clone()
        {
            return new LocationInfo
            {
                Angle = Angle,
                Distance = Distance,
                Latitude = Latitude,
                Longitude = Longitude,
                LatitudeStr = (string)LatitudeStr.Clone(),
                LongitudeStr = (string)LongitudeStr.Clone(),
                ViewCoordField = ViewCoordField
            };
        }

        public bool EqualTo(LocationInfo model)
        {
            return Angle == model.Angle 
                   && Distance == model.Distance 
                   && Latitude == model.Latitude
                   && Longitude == model.Longitude
                   && LatitudeStr == model.LatitudeStr
                   && LongitudeStr == model.LongitudeStr;
        }

        public void Update(LocationInfo model)
        {
            Angle = model.Angle;
            Distance = model.Distance;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            LatitudeStr = model.LatitudeStr;
            LongitudeStr = model.LongitudeStr;
            ViewCoordField = model.ViewCoordField;
        }
    }
}

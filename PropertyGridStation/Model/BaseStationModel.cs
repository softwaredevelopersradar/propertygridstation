﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace PropertyGridStation.Model
{


    public class BaseStationModel : IModelMethods<BaseStationModel>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };


        public BaseStationModel()
        {
            LocationInfo = new LocationInfo();
            Common = new Common();

            Common.PropertyChanged += PropertyChanged;
            LocationInfo.PropertyChanged += PropertyChanged;
        }

        protected void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(LocationInfo))]
        [DisplayName(" ")]
        public LocationInfo LocationInfo { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        public BaseStationModel Clone()
        {
            return new BaseStationModel
            {
                Common = Common.Clone(),
                LocationInfo = LocationInfo.Clone()
            };
        }

        public bool EqualTo(BaseStationModel model)
        {
            return Common == model.Common
                   && LocationInfo == model.LocationInfo;
        }

        public void Update(BaseStationModel model)
        {
            Common.Update(model.Common);
            LocationInfo.Update(model.LocationInfo);
        }
    }
}

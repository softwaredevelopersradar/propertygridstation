﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace PropertyGridStation.Model
{

    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(LocationInfo), 2)]
    [CategoryOrder(nameof(Connection), 3)]

    public class StationModel : BaseStationModel, IModelMethods<StationModel>
    {
        public StationModel():base()
        {
            Connection = new Connection();

            Connection.PropertyChanged += PropertyChanged;
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Connection))]
        [DisplayName(" ")]
        public Connection Connection { get; set; }

        public StationModel Clone()
        {
            return new StationModel
            {
                Common = Common.Clone(),
                Connection = Connection.Clone(),
                LocationInfo = LocationInfo.Clone()
            };
        }

        public bool EqualTo(StationModel model)
        {
            return Common.EqualTo(model.Common)
                   && Connection.EqualTo(model.Connection)
                   && LocationInfo.EqualTo(model.LocationInfo);
        }

        public void Update(StationModel model)
        {
            Common.Update(model.Common);
            Connection.Update(model.Connection);
            LocationInfo.Update(model.LocationInfo);
        }
    }
}

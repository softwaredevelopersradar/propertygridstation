﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using System.Xml.Linq;

namespace PropertyGridStation.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private byte _number = 0;
        private string _note = "";
        private AccessTypes access = AccessTypes.User;

        [NotifyParentProperty(true)]
        [PropertyOrder(0)]
        public byte Number
        {
            get => _number;
            set
            {
                if (_number == value) return;
                _number = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [PropertyOrder(1)]
        public string Note
        { 
            get => _note;
            set
            {
                if (_note == value) return;
                _note = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public AccessTypes Access
        {
            get => access;
            set
            {
                if(access == value) return;
                access = value;
                OnPropertyChanged();
            }
        }

        public override Common Clone()
        {
            return new Common
            {
                Note = Note,
                Number = Number,
                Access = Access,
            };
        }

        public override bool EqualTo(Common model)
        {
            return Number == model.Number
                   && Note == model.Note
                   && Access == model.Access;
        }

        public override void Update(Common model)
        {
            Number = model.Number;
            Note = model.Note;
            Access = model.Access;
        }
    }
}

﻿using CoordFormatLib;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace PropertyGridStation.Model
{
    public abstract class AbstractModelWithCoordTranslation : INotifyPropertyChanged, IDataErrorInfo
    {

        #region UnvisibleProperties
        private ViewCoord viewCoord = ViewCoord.Dd;
        private double _latitude = -1;
        private double _longitude = -1;

        [YamlIgnore]
        [Browsable(false)]
        public ViewCoord ViewCoordField
        {
            get { return viewCoord; }
            set
            {
                if (viewCoord == value) return;
                viewCoord = value;
                ConvertDoubleToCoord();
                OnPropertyChanged();
            }
        }


        [YamlIgnore]
        [Range(-90, 90)]
        [Browsable(false)]
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                ConvertDoubleToCoord();
                OnPropertyChanged();
            }
        }


        [YamlIgnore]
        [Range(-180, 180)]
        [Browsable(false)]
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                ConvertDoubleToCoord();
                OnPropertyChanged();
            }
        }

        #endregion

        #region VisibleProperties

        private string str1 = "-1";
        private string str2 = "-1";
        private char[] mass = new char[] { '°', '\'', '"' };

        [Required]
        [NotifyParentProperty(true)]
        [Category("Coordinates")]
        [RegularExpression("^-{0,1}((90|[0]?[0-9]{1}|[1-8]{1}[0-9]{1})($|[°]{1}$|([.|,]{1}($|[0-9]{1,6}($|[°]{1}$)))|[°]{1}(60$|60[\"|']{1}$|([0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[\"|']{1}$|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|([\"|']{1}[0-9]$|[\"|']{1}(60$|60[\"|']{1}$|[0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|[\"|']{1}$))))))", ErrorMessage = "XXX°XX'XX.XXX\"")]
        public string LatitudeStr
        {

            get { return str1; }
            set
            {
                if (str1 == value) return;
                str1 = value;

                if (str1.IndexOf("-") > -1)
                    _latitude = ConvertCoordToDouble(-1, str1);
                else
                    _latitude = ConvertCoordToDouble(1, str1);

                OnPropertyChanged();
            }
        }

        [Required]
        [NotifyParentProperty(true)]
        [Category("Coordinates")]
        [RegularExpression("^-{0,1}((180|[0]{0,2}[0-9]{1}|[0]?[0-9]{2}|[1]{1}[0-7]{1}[0-9]{1})($|[°]{1}$|([.|,]{1}($|[0-9]{1,6}($|[°]{1}$)))|[°]{1}(60$|60[\"|']{1}$|([0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[\"|']{1}$|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|([\"|']{1}[0-9]$|[\"|']{1}(60$|60[\"|']{1}$|[0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|[\"|']{1}$))))))", ErrorMessage = "XXX°XX'XX.XXX\"")]
        public string LongitudeStr
        {
            get { return str2; }
            set
            {
                if (str2 == value) return;
                str2 = value;

                if (str2.IndexOf("-") > -1)
                    _longitude = ConvertCoordToDouble(-1, str2);
                else
                    _longitude = ConvertCoordToDouble(1, str2);
                OnPropertyChanged();
            }
        }
        #endregion

        #region ConvertCoordFunction
        private double ConvertCoordToDouble(int PositiveOrNegativ, string str)
        {
            double coord = 0;
            try
            {
                str = SubstituteSeparator(str);
                string[] numbers = str.Split(mass);
                if (numbers.Length <= 1)
                    coord = Convert.ToDouble(str);
                else if (numbers.Length == 2 && numbers[1] == "")
                    coord = Convert.ToDouble(str.Trim(mass));
                else if ((numbers.Length == 3 && numbers[2] == "") || (numbers.Length == 2 && numbers[1] != ""))
                    coord = Convert.ToDouble(numbers[0].Trim(mass)) + PositiveOrNegativ * Convert.ToDouble(numbers[1].Trim(mass)) * (1.0 / 60.0);
                else if (numbers.Length == 4 || (numbers.Length == 3 && numbers[2] != ""))
                    coord = Convert.ToDouble(numbers[0].Trim(mass)) + PositiveOrNegativ * Convert.ToDouble(numbers[1].Trim(mass)) * (1.0 / 60.0) + PositiveOrNegativ * Convert.ToDouble(numbers[2].Trim(mass)) * (1.0 / 3600.0);
            }
            catch (Exception)
            {
            }

            return coord;
        }
        private string CutRightZerosOfString(string str)
        {
            if (str.Contains(",") == true)
                str.TrimEnd('0');
            return str;
        }

        private void ConvertDoubleToCoord()
        {

            VCFormat VCoordSoofingGNSS = new VCFormat(_latitude, _longitude);

            switch (viewCoord)
            {
                case ViewCoord.DMm:
                    if (Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Latitude.Minute), 4) >= 60)
                        LatitudeStr = (VCoordSoofingGNSS.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Latitude.Minute) - 60, 4).ToString()) + "'";
                    else LatitudeStr = (VCoordSoofingGNSS.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Latitude.Minute), 4).ToString()) + "'";
                    if (Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Longitude.Minute), 4) >= 60)
                        LongitudeStr = (VCoordSoofingGNSS.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";
                    else LongitudeStr = (VCoordSoofingGNSS.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMin.Longitude.Minute), 4).ToString()) + "'";
                    break;

                case ViewCoord.DMSs:
                    if (Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Latitude.Second)) >= 60)
                        LatitudeStr = (VCoordSoofingGNSS.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Latitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Latitude.Second) - 60).ToString()) + "\"";
                    else LatitudeStr = (VCoordSoofingGNSS.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Latitude.Second)).ToString()) + "\"";
                    if (Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Longitude.Second)) >= 60)
                        LongitudeStr = (VCoordSoofingGNSS.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else LongitudeStr = (VCoordSoofingGNSS.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSoofingGNSS.coordDegMinSec.Longitude.Second)).ToString()) + "\"";
                    break;

                case ViewCoord.Dd:
                    LatitudeStr = CutRightZerosOfString(Math.Round(VCoordSoofingGNSS.coordDeg.Latitude.Degree, 6).ToString()) + "°";
                    LongitudeStr = CutRightZerosOfString(Math.Round(VCoordSoofingGNSS.coordDeg.Longitude.Degree, 6).ToString()) + "°";
                    break;

                default: break;
            }

        }

        private static string SubstituteSeparator(string text)
        {
            char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
            string Source = text.Replace(',', separator);
            Source = Source.Replace('.', separator);
            Source = RemoveAdditionalSepparators(Source, separator);
            return Source;
        }

        private static string RemoveAdditionalSepparators(string text, char separator)
        {
            int index = text.IndexOf(separator);
            text = text.Trim(separator);
            if (index != -1)
                if (!text.Contains(separator))
                    text = text.Insert(index, separator.ToString());
            return text;
        }
        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IDataErrorInfo

        [YamlIgnore]
        [Browsable(false)]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        [YamlIgnore]
        [Browsable(false)]
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                List<ValidationResult> results = new List<ValidationResult>();
                ValidationContext context = new ValidationContext(this);
                if (!Validator.TryValidateObject(this, context, results, true))
                {
                    foreach (var err in results)
                    {
                        if (err.MemberNames.Contains(columnName))
                            error = err.ErrorMessage;
                    }
                }
                return error;
            }
        }

        #endregion

    }
}

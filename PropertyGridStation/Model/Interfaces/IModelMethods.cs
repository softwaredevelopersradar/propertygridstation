﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridStation.Model
{
    public interface IModelMethods<T> where T : class
    {
        bool EqualTo(T model);

        T Clone();

        void Update(T model);
    }
}

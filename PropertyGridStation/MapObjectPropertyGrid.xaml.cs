﻿using PropertyGridStation.Editors;
using PropertyGridStation.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using Key = System.Windows.Input.Key;

namespace PropertyGridStation
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class MapObjectPropGrid : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<StationModel> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<StationModel> OnApplyButtonClick = (sender, obj) => { };
        public event EventHandler<StationModel> OnDeleteButtonClick = (sender, obj) => { };
        public event EventHandler<StationModel> OnExitButtonClick = (sender, obj) => { };
        #endregion

        #region Properties


        #region Visibility

        public static readonly DependencyProperty DependencyAccess =
       DependencyProperty.Register("Access", typeof(AccessTypes), typeof(MapObjectPropGrid), new PropertyMetadata(AccessTypes.User, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        public static readonly DependencyProperty DependencyLanguage =
            DependencyProperty.Register("Language", typeof(Language), typeof(MapObjectPropGrid), new PropertyMetadata(Language.EN, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsEthernetExist =
           DependencyProperty.Register("VisibilityEthernet", typeof(bool), typeof(MapObjectPropGrid), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsRadioExist =
            DependencyProperty.Register("VisibilityRadio", typeof(bool), typeof(MapObjectPropGrid), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityIsGSMExist =
            DependencyProperty.Register("VisibilityGSM", typeof(bool), typeof(MapObjectPropGrid), new PropertyMetadata(true, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        public bool VisibilityEthernet
        {
            get => (bool)GetValue(dependencyVisibilityIsEthernetExist);
            set => SetValue(dependencyVisibilityIsEthernetExist, value);
        }

        public bool VisibilityRadio
        {
            get => (bool)GetValue(dependencyVisibilityIsRadioExist);
            set => SetValue(dependencyVisibilityIsRadioExist, value);
        }

        public bool VisibilityGSM
        {
            get => (bool)GetValue(dependencyVisibilityIsGSMExist);
            set => SetValue(dependencyVisibilityIsGSMExist, value);
        }

        public AccessTypes Access
        {
            get => (AccessTypes)GetValue(DependencyAccess);
            set => SetValue(DependencyAccess, value);
        }

        public Language Language
        {
            get => (Language)GetValue(DependencyLanguage);
            set => SetValue(DependencyLanguage, value);
        }

        private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MapObjectPropGrid mapObjectPropGrid = d as MapObjectPropGrid;
            mapObjectPropGrid.OnDependencyPropertyChanged(e);
        }

        private void OnDependencyPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(Language):
                    ChangeLanguage((Language)e.NewValue);
                    break;
                case nameof(Access):
                    Local.Common.Access = (AccessTypes)e.NewValue;
                    break;
                case nameof(VisibilityEthernet):
                    Local.Connection.IsEthernetExist = (bool)e.NewValue;
                    break;
                case nameof(VisibilityRadio):
                    Local.Connection.IsRadioExist = (bool)e.NewValue;
                    break;
                case nameof(VisibilityGSM):
                    Local.Connection.IsGSMExist = (bool)e.NewValue;
                    break;
            }
        }


        #endregion



        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private StationModel savedLocal;
        public StationModel Local
        {
            get => (StationModel)Resources["localProperties"];
            set
            {
                value.Common.Access = Access;
                value.Connection.IsEthernetExist = VisibilityEthernet;
                value.Connection.IsGSMExist = VisibilityGSM;
                value.Connection.IsRadioExist = VisibilityRadio;
                if ((StationModel)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    savedLocal.Connection.IsEthernetExist = VisibilityEthernet;
                    savedLocal.Connection.IsGSMExist = VisibilityGSM;
                    savedLocal.Connection.IsRadioExist = VisibilityRadio;
                    return;
                }
                ((StationModel)Resources["localProperties"]).Update(value); ;
                savedLocal = value.Clone();
            }
        }
        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Language newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Language newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Language.EN:
                        dict.Source = new Uri("/PropertyGridStation;component/Language/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Language.RU:
                        dict.Source = new Uri("/PropertyGridStation;component/Language/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Language.AZ:
                        dict.Source = new Uri("/PropertyGridStation;component/Language/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/PropertyGridStation;component/Language/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher" || property.Name == "BRD"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            //  Console.WriteLine(property.Name+ "  " + subProperty.Name);
                            continue;
                        }

                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            funcValidation(Local);

        }

        #endregion


        public MapObjectPropGrid()
        {
            InitializeComponent();
            InitLocalProperties();
            ChangeLanguage(Language);
            Validate();
        }


        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            Local.OnPropertyChanged += OnPropertyChanged;

            
            PropertyLocal.Editors.Add(new EditorCustom(nameof(StationModel.LocationInfo), typeof(StationModel)));
            PropertyLocal.Editors.Add(new EditorCustom(nameof(StationModel.Connection), typeof(StationModel)));
            PropertyLocal.Editors.Add(new EditorCustom(nameof(StationModel.Common), typeof(StationModel))); 
        
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Local.Common.Access = Access;
            Local.Connection.IsEthernetExist = VisibilityEthernet;
            Local.Connection.IsGSMExist = VisibilityGSM;
            Local.Connection.IsRadioExist = VisibilityRadio;
            Validate();
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            OnApplyButtonClick(this, Local);
        }

        private void butDelete_Click(object sender, RoutedEventArgs e)
        {
            OnDeleteButtonClick(this, Local);
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                this.OnApplyButtonClick(this, Local);
            else if (e.Key == Key.Escape)
                this.OnDeleteButtonClick(this, Local);
        }

        private void butExit_Click(object sender, RoutedEventArgs e)
        {
            OnExitButtonClick(sender, Local);
        }

        private void Grid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta / 3);
        }
    }
}
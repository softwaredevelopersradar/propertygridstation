﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PropertyGridStation.Converters
{
    public class NumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (System.Convert.ToByte(value))
            {
                case 1: return "98";
                case 2: return "99";
                case 3: return "0";
                case 4: return "1";
                case 5: return "2";
                case 6: return "3";
                case 7: return "4";
                case 8: return "5";
                case 9: return "6";
                case 10: return "7";
            }


            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

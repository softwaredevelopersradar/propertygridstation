﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PropertyGridStation.Command
{
   public class UpCommand
    {
        private static RelayCommand executeCommandUp;

        public static RelayCommand ExecutedCommandUp
        {
            get
            {
                return executeCommandUp ??
                    (executeCommandUp = new RelayCommand(ShowDialog, CanExecute));
            }
            set
            {
                if (executeCommandUp == value) return;
                executeCommandUp = value;
            }
        }


        public static void ShowDialog(object e)
        {
            int num = 0;
            try
            {
                num = Convert.ToInt32((e as TextBox).Text);
            }
            catch (Exception) { }

            if (num >= 9) return;

            (e as TextBox).Text = Convert.ToString(num + 1);
        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }


    public class DownCommand
    {
        private static RelayCommand executeCommandDpwn;

        public static RelayCommand ExecutedCommandDown
        {
            get
            {
                return executeCommandDpwn ??
                    (executeCommandDpwn = new RelayCommand(ShowDialog, CanExecute));
            }
            set
            {
                if (executeCommandDpwn == value) return;
                executeCommandDpwn = value;
            }
        }


        public static void ShowDialog(object e)
        {
            int num = 0;
            try
            {
                num = Convert.ToInt32((e as TextBox).Text);              
            }
            catch (Exception) { }

            if (num <= 0) return;

            (e as TextBox).Text = Convert.ToString(num - 1);
        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}

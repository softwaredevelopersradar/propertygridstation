﻿using PropertyGridStation.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;


namespace PropertyGridStation.Editors
{
    public class EditorCustom : PropertyEditor
    {
        private Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(StationModel.Common), "CommonEditorKey" },
            { nameof(StationModel.Connection), "ConnectionEditorKey" },
            { nameof(StationModel.LocationInfo), "CoordinatesEditorKey" }
        };



        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PropertyGridStation;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }

        public EditorCustom(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PropertyGridStation;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }

    }
}
